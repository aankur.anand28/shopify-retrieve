import requests
import mysql.connector
import time
import config
from datetime import datetime
import db_interaction
import shopify
import binascii
import os


def placeOrder(items):
    """
    :param items: list of items to be created
    :return: whether item has been created or not
    """
    #1. Place order on api
    #2. Add the same on DB with ID clause


def getProduct():
    """
    :return: retrieve product from DB
    """
    # print("Running")
    try:
        endpoint = 'products.json'
        r = requests.get(config.url + endpoint)
        # print(r.json())

        return r.json()
    except Exception as e:
        print(e)
        print("Exception in getProduct")


def change_status(productId, status):
    endpoint = 'products/'
    payload = {
        "product": {
            "status": status,
        }
    }
    final_url = config.url+endpoint+str(productId)+'.json'
    r = requests.put(final_url, json = payload)
    # print(r.json())
    return r.json()


def listProducts():
    """
    :return: list of Products in DB
    """
    try:
        cnx = mysql.connector.connect(user=config.username, password=config.password, database=config.database)
        cursor = cnx.cursor()
        select_products = (
            "SELECT * FROM products"
        )
        # check_data_product = (productId,)
        cursor.execute(select_products)
        result = cursor.fetchall()
        if result:
            resp = {"Status": "Success", "Message": "Successfully fetched", "Data": result}
        else:
            print("No value in DB")
            resp = {"Status": "Success", "Message": "Successfully fetched", "Data": result}
        return resp

    except Exception as e:
        print("Make sure you've run cron-job.py first")
        print("Exception raised in listProducts")
        return


def authShopify():
    shopify.Session.setup(api_key=config.API_KEY, secret=config.Password)
    shop_url = config.Store_URL
    api_version = config.api_version
    state = binascii.b2a_hex(os.urandom(15)).decode("utf-8")
    redirect_uri = "http://myapp.com/auth/shopify/callback"
    scopes = ['read_products', 'read_orders']
    newSession = shopify.Session(shop_url, api_version)
    auth_url = newSession.create_permission_url(scopes, redirect_uri, state)

    return auth_url


def makeSession():
    session = shopify.Session(config.Store_URL, config.api_version)
    access_token = session.request_token(request_params)


_now = datetime.now()
current_time = _now.strftime("%HH")

if __name__ == '__main__':
    # should be in form of executing localhost:/

    if current_time < '12H':
        print("Good Morning")
    elif current_time < '16H':
        print("Good Afternoon")
    elif '3H' > current_time > '22H':
        print("Late evenings!")
    else:
        print("Good Evening")
    print()
    print()
    print("Let's Start")
    print("To exit press Ctrl-C")
    print()
    try:
        while 5:
            print()
            print()
            print("\t1. Fetch products from DB.\n\t2. Place an order.\n\t3. For Fetching in Date Range. \n\t 4.To exit, press 0.")
            client_input = int(input())
            if client_input == 1:
                print()
                print('127.0.0.1:8000/listProducts')
                print()
                time.sleep(1)
                response = listProducts()
                print(response)
                time.sleep(1)
                print()
            elif client_input == 2:
                print()
                print('127.0.0.1:8000/placeOrder')
                time.sleep(1)
                # repsonse = placeOrder()
                print()
            elif client_input == 3:
                print()
                print('127.0.0.1:8000/fetchinrange')
                time.sleep(1)
                start_date = input('Enter Start Date (in YYYY-MM-DD):')
                end_date = input('Enter End Date (in YYYY-MM-DD):')
                repsonse = db_interaction.fetch_in_range(start_date, end_date)
                print()
            elif client_input == 0:
                print()
                print("Thank you for your time")
                break
            else:
                print("Invalid Input")
            time.sleep(5)
    except KeyboardInterrupt:
        print()
        print("Hope you had a good time!")


# debugging tips:
# Try running it individually :)
# listProducts()
# placeOrder()
# print(authShopify())
# print(makeSession())
# change_status(123,'active')
