import mysql.connector
from mysql.connector import errorcode
import config


def createTable():
    """
    :return: Table created or not
    """
    try:

        cnx = mysql.connector.connect(user=config.username,password=config.password,database=config.database)
        cursor = cnx.cursor()

        TABLES = {}
        TABLES['products'] = (
            "CREATE TABLE `products` ("
            "  `id` bigint(20) NOT NULL AUTO_INCREMENT,"
            "  `productID` bigint(20) NOT NULL,"
            "  `title` varchar(200) DEFAULT NULL,"
            "  `body_html` varchar(500) DEFAULT NULL,"
            "  `vendor` varchar(300) DEFAULT NULL,"
            "  `product_Type` varchar(100) DEFAULT NULL,"
            "  `status` enum('failed','active','draft', 'succeeded') DEFAULT 'draft',"
            "  `handle` varchar(300) NOT NULL,"
            "  `product_date` date DEFAULT NULL,"
            "  `updated_on` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,"
            "  PRIMARY KEY (`id`,`productID`)"
            ") ENGINE=InnoDB")

        for table_name in TABLES:
            table_description = TABLES[table_name]
            try:
                print("Creating table {}: ".format(table_name), end='')
                cursor.execute(table_description)
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
                    print("already exists.")
                else:
                    print(err.msg)
            else:
                print("OK")

        cursor.close()
        cnx.close()
        return
    except Exception as e:
        print("Exception raised while creating Table.\n Make sure you've entered correct credentials in config file")
        return e


def insert_into_db(data_product):
    """
    :params data_product: containing the data to be inserted in DB
    :return: value inserted into DB with status
    """
    cnx = mysql.connector.connect(user=config.username, password=config.password, database=config.database)
    cursor = cnx.cursor()


    add_products = ("INSERT INTO products "
                    "(productID, title, body_html, vendor, product_Type, status, handle, product_date) "
                    "VALUES (%s, %s, %s, %s, %s, %s, %s, %s)")


    # Update new values
    cursor.execute(add_products, data_product)
    ID = cursor.lastrowid
    print(ID)

    # Make sure data is committed to the database
    cnx.commit()

    cursor.close()
    cnx.close()

    resp = {"Status": "Success", "Message": "Data Inserted into DB"}

    return resp


def update_db(data_product):
    """
    :params data_product: containing the data to be updated to DB
    :return: Updates the value
    """
    try:
        cnx = mysql.connector.connect(user=config.username, password=config.password, database=config.database)
        cursor = cnx.cursor()

        update_products = ("UPDATE products SET title = %s, body_html =%s, vendor=%s, product_Type=%s, status=%s, handle=%s, product_date=%s "
                        "WHERE productId = %s")

        # Update new values
        cursor.execute(update_products, data_product)
        # ID = cursor.lastrowid
        # print(ID)

        # Make sure data is committed to the database
        cnx.commit()
        cursor.close()
        cnx.close()

        resp = {"Status": "Success", "Message": "Data Updated into DB"}

    except Exception as e:
        #print(e)
        print("Exception in Update DB")
        return e

    return resp


def fetch_in_range(start_date, end_date):
    """
    :param start_date: start date (excluding)
    :param end_date: end date (excluding)
    :return:
    """
    try:
        cnx = mysql.connector.connect(user=config.username, password=config.password, database=config.database)
        cursor = cnx.cursor()

        select_products = (
            "SELECT * FROM products WHERE product_date > %s AND product_date < %s"
        )
        check_data_product = (start_date,end_date, )
        cursor.execute(select_products, check_data_product)
        result = cursor.fetchall()
        if result:
            print(result)
        else:
            print("Sorry! No values available")
    except Exception as e:
        print("Problem in fetch_in_range")
        print(e)

    return

# debugging tips:
# Try running it individually :)
# createTable()
# while testing update_db and insert_db remember to remove field_names and only keep values
# insert_into_db((productId=123, title='abc', body_html='mno', vendor='gullu', product_type='working', status='draft', handle='kone', product_date='2021-09-25'))
# update_db((title='abc', body_html='mno', vendor='binny', product_type='working', status='active', handle='won', product_date='2021-09-25', productId=123))
# update_db((title='abc', body_html='potus', vendor='briji', product_type='working', status='active', handle='toby', product_date='2021-09-25', productId=122))
# fetch_in_range('2021-09-23','2021-09-26')
