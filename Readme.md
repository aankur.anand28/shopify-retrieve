<h1><center>DOCUMENTATION</center></h1>
<br>
<br>
<h3>APIs:</h3>
<ol>
<li>listProducts</li>
<li>placeOrder</li>
<li>fetchinrange</li>
</ol>
<br>
<h3>Installation:</h3>
<li>Make a virtual environment in python</li>
<li>Run pip3 install -r requirements.txt</li>
<br>
<h3>Working:</h3>
<ol>
<li>listProducts: selects all the elements present currently in DB.</li>
<li>placeOrder: will place the order and store the product necessary details in DB.</li>
<li>fetchinrange: will fetch the products in a range excluding the given dates as input present in DB.</li>
<li>cron-job.py: can update or insert value in DB depending whether the value is present in DB or not. (Major work)</li>
</ol>

<h3><b> Steps to Execute: </b></h3>
<ol>
<li>Create a schema with name BE and update the values in config file.</li>
<li>Execute cron-job.py to create and sync DB with the link containing products.</li>
<li>Execute main.py with the api you want to call.
    
2.1 listProducts

2.2 placeOrder</li>
</ol>
<br>
<h5> How to execute cron-job? </h5>
<p> > Run python3 cron-job.py file</p>

<br>
<h5> How to execute main? </h5>
<p> > Run python3 main.py file and you'll get the required option to choose.</p>
<br>
<br>
<p><i>Function description can be found below every function call and debugging tips can be found at the footer of all files.</i></p>
