import mysql.connector
import config
import main
from datetime import datetime
import db_interaction
import requests

# Logic:
# 1. Check if productID present in DB or not
# 2. If not, insert value into db
# 3. If present, update value if any changes


def check_db():
    """
    :return: checking the value present in db or not
    """
    try:
        cnx = mysql.connector.connect(user=config.username, password=config.password, database=config.database)
        cursor = cnx.cursor()

        products = main.getProduct()
        # print(products)
        for prod in products['products']:
            productId = prod['id']
            title = prod['title']
            body_html = prod['body_html']
            vendor = prod['vendor']
            status = prod['status']
            product_type = prod['product_type']
            handle = prod['handle']
            product_date = prod['updated_at'].split('T')[0]
            print(productId, title, body_html, vendor, product_type, handle, product_date)

            data_product = (productId, title, body_html, vendor, product_type, status, handle, product_date)
            update_data_product = (title, body_html, vendor, product_type, status, handle, product_date, productId)
            select_products = (
                "SELECT * FROM products WHERE productID = %s"
            )
            check_data_product = (productId, )
            cursor.execute(select_products, check_data_product)
            result = cursor.fetchall()
            if result:
                print(db_interaction.update_db(update_data_product))
            else:
                print(db_interaction.insert_into_db(data_product))

        # resp = {"Status": "Success", "Message": "Check DB function executed properly"}

    except Exception as e:
        print("Exception raised in check DB")
        return e

    return "Success"


_now = datetime.now()
# print(_now.strftime("%HH"))
db_interaction.createTable()
current_time = _now.strftime("%HH")
if current_time < '12H':
    print("Good Morning")
elif current_time < '16H':
    print("Good Afternoon")
elif '3H' > current_time > '22H':
    print("Late evenings!")
else:
    print("Good Evening")
print()
print()
print("Let's Start")
print("To exit press Ctrl-C")
print()
try:
    while True:
        print(check_db())
except KeyboardInterrupt:
    print()
    print("Hope you had a good time!")


# debugging tips:
# Try running it individually :)
# check_db()
